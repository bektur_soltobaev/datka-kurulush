const bt1 = document.getElementById("btn1");
const bt2 = document.getElementById("btn2");
const bt3 = document.getElementById("btn3");
const bt4 = document.getElementById("btn4");

const img1 = document.getElementById("gallery-img1");
const img2 = document.getElementById("gallery-img2");
const img3 = document.getElementById("gallery-img3");
const img4 = document.getElementById("gallery-img4");

bt1.onmouseover = function() {
  img1.style.transform = "scaleY(1)";
  img1.style.bottom = "0";

  img2.style.transform = "scaleY(0)";
  img3.style.transform = "scaleY(0)";
  img4.style.transform = "scaleY(0)";
};

bt2.onmouseover = function() {
  img2.style.transform = "scaleY(1)";

  img1.style.transform = "scaleY(0)";
  img3.style.transform = "scaleY(0)";
  img4.style.transform = "scaleY(0)";
};

bt3.onmouseover = function() {
  img3.style.transform = "scaleY(1)";

  img2.style.transform = "scaleY(0)";
  img1.style.transform = "scaleY(0)";
  img4.style.transform = "scaleY(0)";
};

bt4.onmouseover = function() {
  img4.style.transform = "scaleY(1)";

  img2.style.transform = "scaleY(0)";
  img3.style.transform = "scaleY(0)";
  img1.style.transform = "scaleY(0)";
};

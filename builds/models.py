from django.db import models


STATUS_CHOICES = {
    (1, "Свободно"),
    (2, "Куплено"),
    (3, "В рассрочке"),
    (4, "Есть скидка")
}


class Apartment(models.Model):
    number = models.PositiveIntegerField(verbose_name='номер')
    floor = models.ForeignKey('Floor', on_delete=models.CASCADE,
                              verbose_name='этаж', related_name='appartment')
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)

    def __str__(self):
        return 'Номер: ' + self.number + " Этаж: " + self.floor.number


class Floor(models.Model):
    number = models.PositiveIntegerField(verbose_name='номер')

    def __str__(self):
        return "Этаж " + self.number

from django.contrib import admin
from .models import Apartment, Floor

admin.site.register([Apartment, Floor])
